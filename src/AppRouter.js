import React, { Component } from "react";
// import HomeScreen from "./HomeScreen.js";
// import MainScreenNavigator from "../ChatScreen/index.js";
// import ProfileScreen from "../ProfileScreen/index.js";
import HomeScreen from './modules/appscreens/Home';
import ListScreen from './modules/appscreens/stack';
// import DetailScreen from './modules/appscreens/ItemDetail';
import SideBar from "./modules/global/components/Sidebar";
import { DrawerNavigator } from "react-navigation";

const HomeScreenRouter = DrawerNavigator(
  {
    Home: { screen: HomeScreen },
    List: { screen: ListScreen },
    // Detail: { screen: DetailScreen }
  },
  {
    contentComponent: props => <SideBar {...props} />
  }
);
export default HomeScreenRouter;