import React, { Component } from "react";
import ListScreen from "./Item";
import DetailScreen from "./ItemDetail";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator({
    ListScreen: { 
    	screen: ListScreen,
    	navigationOptions: {
	      headerLeft: null
	    }, 
	},
    Detail: { 
    	screen: DetailScreen,
    	navigationOptions: {
	      headerLeft: null
	    }, 
	}
},
  {
    initialRouteName: "ListScreen"
  },
  { headerMode: 'modal' }
));
