import React from "react";
import { Image, ScrollView } from "react-native";
import {
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  View,
  Container,
  Header,
  Title,
  H1
} from "native-base";
import * as itemActions from "../../../actions/Item/ItemActions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

class ItemDetail extends React.Component {
  static navigationOptions = {
    header: null
  };
  componentWillMount() {
    const { params } = this.props.navigation.state;
    this.props.dispatchFetchSingleItem(params.storyId);
  }
  render() {
    const { title, description, ...props } = this.props.itemDetail;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Detail</Title>
          </Body>
          <Right />
        </Header>
        <ScrollView>
          <Card>
            <CardItem>
              <Body>
                <H1>{title}</H1>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{description}</Text>
              </Body>
            </CardItem>
            {this.props.itemDetail.cards.map(card => {
              return (
                <View cardBody key={card.card_id}>
                  <Image
                    source={{ uri: `${card.card_url.medium}` }}
                    style={{ height: 200, width: null, flex: 1 }}
                  />
                  <Text>{card.description}</Text>
                </View>
              );
            })}
          </Card>
        </ScrollView>
      </Container>
    );
  }
}

ItemDetail.defaultProps = {
  itemDetail: {
    cards: []
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    itemDetail: state.item.singleItem
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dispatchFetchSingleItem: itemActions.FetchSingleItem
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetail);
