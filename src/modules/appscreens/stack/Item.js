import React from 'react';
import { Image,ScrollView } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, Container,Header,Title } from 'native-base';
import * as itemActions from '../../../actions/Item/ItemActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Item extends React.Component{
	static navigationOptions = {
    header: null
  }
	componentDidMount()
	{
		const { params } = this.props.navigation.state;
		params&&this.props.dispatchFetchItem(`?category_id=${params.categoryId}&limit=10`);

	}
	componentWillReceiveProps(nextProps, prevState)
	{
		// if (nextProps.navigation.state.params.categoryId !== this.props.navigation.state.params.categoryId) {
		// 	console.log('getDerivedStateFromPropsIN')
		// 	this.props.dispatchFetchItem(`?category_id=${nextProps.navigation.state.params.categoryId}&limit=10`)			
		// }
	}
	openCard=(id)=>{
		this.props.navigation.navigate('Detail', {storyId:id});
	}
	render()
	{
		const { params } = this.props.navigation.state;
		if (params) {
			return(
			<Container>
				<Header>
                  <Left>
                    <Button
                      transparent
                      onPress={() => this.props.navigation.navigate("DrawerOpen")}>
                      <Icon name="menu" />
                    </Button>
                  </Left>
                  <Body>
                    <Title>{params?params.categoryTitle:''}</Title>
                  </Body>
                  <Right />
                </Header>
				<ScrollView>
					<Grid>
					{this.props.item.map(item=>{
						return(
							<Row key={item.contentsId}>
								<Card>
									<CardItem>
										<Left>
											<Body>
												<Text>{item.title}</Text>
											</Body>
										</Left>
									</CardItem>
									<CardItem cardBody button onPress={()=>this.openCard(item.contentsId)}>
										<Image source={{uri: `${item.thumbnailUrl}`}} style={{height: 100, width: null, flex: 1}}></Image>
									</CardItem>
									<CardItem>
										<Left>
											<Icon style={{fontSize:17}} active name="thumbs-up" />
	                  						<Text style={{fontSize:12}}>{item.likeCount} Likes</Text>
										</Left>
										<Body style={{flex: 1, flexDirection: 'row'}}>
											<Icon style={{fontSize:17,marginRight:10}} active name="eye" />
	                  						<Text style={{fontSize:12}}>{item.viewCount} Viewers</Text>
										</Body>
										<Right>
	                  						<Text style={{fontSize:12}}>{item.udate}</Text>
										</Right>
									</CardItem>
								</Card>
							</Row>
					)
					})}
					</Grid>
				</ScrollView>
			</Container>
			)
		}
		else
		{
			return(<Container></Container>)
		}
	}
}

Item.defaultProps={
	item:[],
}

const mapStateToProps = (state,ownProps) => {
  return{
    item: state.item.data,
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    dispatchFetchItem: itemActions.FetchItem,
  },dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (Item)