import React, { PropTypes, Component } from 'react';
import { Container, Header, Content, Button, Text, Left, Right, Icon, Title, Input, Body, View } from 'native-base';
import { StyleSheet } from 'react-native';

class Home extends Component {
    render() {
        return(
            <Container>
                <Header>
                  <Left>
                    <Button
                      transparent
                      onPress={() => this.props.navigation.navigate("DrawerOpen")}>
                      <Icon name="menu" />
                    </Button>
                  </Left>
                  <Body>
                    <Title>HomeScreen</Title>
                  </Body>
                  <Right />
                </Header>
                <Content padder>
                    <View style={styles.container}>
                     <Text style={{fontSize:30}}>Welcome my Friend!</Text>
                     <Text style={{fontSize:20}}>Its app tell you about some story</Text>
                     <Text style={{fontSize:20}}>Please enjoy reading...</Text>
                   </View>
                </Content>                
            </Container>
        )
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:200
  },
});

export default Home;
