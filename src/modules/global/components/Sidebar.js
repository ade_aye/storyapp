import React from "react";
import { AppRegistry, Image, StatusBar } from "react-native";
import {
  Button,
  Text,
  Container,
  List,
  ListItem,
  Content,
  Icon
} from "native-base";
import * as categoryActions from '../../../actions/Categories/categoriesActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { NavigationActions } from "react-navigation";
class SideBar extends React.Component {
  componentWillMount()
  {
    this.props.dispatchFetchCategories();
  }
  render() {
    return (
      <Container>
        <Content>
          <Image
            source={{
              uri:
                "https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/drawer-cover.png"
            }}
            style={{
              height: 120,
              width: "100%",
              alignSelf: "stretch",
              position: "absolute"
            }}
          />
          <Image
            square
            style={{
              height: 80,
              width: 70,
              position: "absolute",
              alignSelf: "center",
              top: 20
            }}
            source={{
              uri:
                "https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/logo.png"
            }}
          />
          <List
            dataArray={this.props.categories}
            contentContainerStyle={{ marginTop: 120 }}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate('ListScreen',{categoryId:data.categoryId, categoryTitle:data.categoryTitle})}
                >
                  <Text>{data.categoryTitle}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state,ownProps) => {
  return{
    categories: state.categories.data,
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    dispatchFetchCategories: categoryActions.FetchCategories,
  },dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (SideBar)