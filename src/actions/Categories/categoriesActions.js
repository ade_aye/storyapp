// require('es6-promise').polyfill();
// import fetch from 'isomorphic-fetch';

import axios from 'axios';

import * as types from '../../constants/actionTypes';
import * as api from '../../constants/api';

export const FetchCategories = (query='',callback) => async (dispatch) =>{
  const response = await axios(`${api.base_url}/categories${query}`);
  // const json = await response.json();
  if (response.data.status === true ) {
    dispatch({
      type: types.SET_CATEGORIES,
      data: response.data.categories,
    });
  }
  else {
    // dispatch({
    //   type: ERROR,
    //   data: JSON.stringify(json),
    // });
    console.log('errr')
  }
  if (callback) {
    callback();
  }
}
