// require('es6-promise').polyfill();
// import fetch from 'isomorphic-fetch';
import axios from 'axios';
import * as types from '../../constants/actionTypes';
import * as api from '../../constants/api';

export const FetchItem = (query='',callback) => async (dispatch) =>{
  const response = await axios(`${api.base_url}/contents${query}`);
  // response = response.data;
  if (response.data.status == true) {
    dispatch({
      type: types.SET_ITEM,
      data: response.data.contents,
    });
  }
  else {
    // dispatch({
    //   type: ERROR,
    //   data: JSON.stringify(json),
    // });
    console.log('errr',response)
  }
  if (callback) {
    callback();
  }
}


export const FetchSingleItem = (id,query='',callback) => async (dispatch) =>{
  const response = await axios(`${api.base_url}/stories/${id}`);
  // const json = await response.json();
  if (response.data.status) {
    dispatch({
      type: types.SET_SINGLE_ITEM,
      data: response.data.story,
    });
  }
  else {
    // dispatch({
    //   type: ERROR,
    //   data: JSON.stringify(json),
    // });
    console.log('errr')
  }
  if (callback) {
    callback();
  }
}