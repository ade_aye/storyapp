import * as types from '../constants/actionTypes';

const initialState = {
	data:[],
}

export default function (state = initialState, action) {
    switch (action.type) {
        case types.SET_CATEGORIES:
			return {
				...state,
				data:action.data
			};
       default:
			return state;
    }
}
