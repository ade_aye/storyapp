import { combineReducers } from 'redux';
import categories from './categoriesReducer';
import item from './itemReducer';

const rootReducer = combineReducers({
	categories,
	item,
});

export default rootReducer;
