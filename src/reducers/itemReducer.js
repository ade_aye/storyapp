import * as types from '../constants/actionTypes';

const initialState = {
	data:[],
	singleItem:{
		cards: [],
	},
}

export default function (state = initialState, action) {
    switch (action.type) {
        case types.SET_ITEM:
			return {
				...state,
				data:action.data
			};
		case types.SET_SINGLE_ITEM:
			return{
				...state,
				singleItem:action.data
		}
       default:
			return state;
    }
}
